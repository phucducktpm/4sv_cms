import { CloseCircleOutlined } from '@ant-design/icons';
import { Button, Card, Col, DatePicker, Form, Input, Popover, Row, Select, TimePicker } from 'antd';

import React, { FC, useState } from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { connect, Dispatch } from 'umi';
import TableForm from './components/TableForm';
import FooterToolbar from './components/FooterToolbar';
import styles from './style.less';

type InternalNamePath = (string | number)[];

const { Option } = Select;
const { RangePicker } = DatePicker;

const fieldLabels = {
  class:'Class',
  university:'University',
  major:'Major',
  name: 'Name',
  email: 'Email',
  gender: 'Gender',
  province: 'Province',
  district: 'District',
  ward: 'Ward',
  birthday: 'Birthday',
  address: 'Address',
  owner: 'Owner',
  approver: 'Approver',
  dateRange: 'Date Range',
  type: 'Type',
  name2: 'Name 2',
  url2: 'Url 2',
  owner2: 'Owner 2',
  approver2: 'Approver 2',
  dateRange2: 'Date Range 2',
  type2: 'Type 2',
};

interface AdvancedFormProps {
  dispatch: Dispatch;
  submitting: boolean;
}

const AdvancedForm: FC<AdvancedFormProps> = ({ submitting, dispatch }) => {
  const [form] = Form.useForm();

  const onFinish = (values: { [key: string]: any }) => {
    console.log(values);
    dispatch({
      type: 'formAndadvancedForm/submitAdvancedForm',
      payload: values,
    });
  };

  const onFinishFailed = (errorInfo: any) => {
  };

  return (
    <Form
      form={form}
      layout="vertical" 
      hideRequiredMark // hide star requited before label
      // initialValues={{ members: tableData }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <PageHeaderWrapper content="Add student xxx yyy">
        <Card title="Add Student" className={styles.card} bordered={false}>
          <Row gutter={8}>
            <Col lg={6} md={12} sm={24}>
              <Form.Item label={fieldLabels.name} name="full_name" rules={[{ required: true, message: 'name required' }]}
              >
                <Input placeholder="full name" />
              </Form.Item>
            </Col>
            <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
              <Form.Item label={fieldLabels.email} name="email" rules={[{ required: true, message: 'email required' }]}
              >
                <Input
                  style={{ width: '100%' }}
                  addonAfter="@gmail.com"
                  placeholder="email"
                />
              </Form.Item>
            </Col>
            <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
              <Form.Item label={fieldLabels.gender} name="gender" rules={[{ required: true, message: 'gender required' }]}
              >
                <Select placeholder="gender">
                  <Option value="0">Nam</Option>
                  <Option value="1">Nữ</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={8}>
            <Col lg={6} md={12} sm={24}>
              <Form.Item label={fieldLabels.university} name="university_id" rules={[{ required: true, message: 'name required' }]}
              >
                <Select placeholder="University">
                  <Option value="0">Nam</Option>
                  <Option value="1">Nữ</Option>
                </Select>
               
              </Form.Item>
            </Col>
            <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
              <Form.Item label={fieldLabels.major} name="major_id" rules={[{ required: true, message: 'email required' }]}
              >
                <Select placeholder="Major">
                  <Option value="0">Nam</Option>
                  <Option value="1">Nữ</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
              <Form.Item label={fieldLabels.class} name="class" rules={[{ required: true, message: 'gender required' }]}
              >
                <Input placeholder="Class" />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={8}>
            <Col lg={6} md={12} sm={24}>
              <Form.Item label={fieldLabels.province} name="address_province_id" rules={[{ required: true, message: 'address_province_id required' }]}
              >
                <Select placeholder="address_province_id">
                  <Option value="1">HCM</Option>
                  <Option value="2">Ha Noi</Option>
                  <Option value="3">Nha Trang</Option>
                </Select>
              </Form.Item>
            </Col>

            <Col xl={{ span: 6, offset: 2 }} lg={{ span: 8 }} md={{ span: 12 }} sm={24}>
              <Form.Item label={fieldLabels.district} name="address_district_id" rules={[{ required: true, message: 'address_district_id required' }]}
              >
                <Select placeholder="address_district_id">
                  <Option value="1">quan x</Option>
                  <Option value="2">quan y</Option>
                  <Option value="3">quan z</Option>
                </Select>
              </Form.Item>
            </Col>

            <Col xl={{ span: 8, offset: 2 }} lg={{ span: 10 }} md={{ span: 24 }} sm={24}>
              <Form.Item label={fieldLabels.ward} name="address_ward_id" rules={[{ required: true, message: 'address_ward_id required' }]}
              >
                <Select placeholder="address_ward_id">
                  <Option value="1">phương x</Option>
                  <Option value="2">phương y</Option>
                  <Option value="3">phương z</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={8}>
            <Col xl={12} sm={24}>
              <Form.Item label={fieldLabels.address} name='address' rules={[{ required: true, message: 'address required' }]}>
                <Input style={{ width: '100%' }} placeholder='address' />
              </Form.Item>
            </Col>
            <Col xl={12} sm={24}>
              <Form.Item label={fieldLabels.birthday} name='birthday' rules={[{ required: true, message: 'birthday required' }]}>
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={8}>
            <Button type="primary" onClick={() => form?.submit()} loading={submitting}>
              Submit
        </Button>
          </Row>
        </Card>

      </PageHeaderWrapper>
    </Form>
  );
};

export default connect(({ loading }: { loading: { effects: { [key: string]: boolean } } }) => ({
  submitting: loading.effects['formAndadvancedForm/submitAdvancedForm'],
}))(AdvancedForm);
